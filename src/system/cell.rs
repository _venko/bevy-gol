use std::collections::HashMap;

use bevy::{
  ecs::{
    entity::Entity,
    system::{ParallelCommands, Query},
  },
  math::IVec2,
};

use crate::{GolCell, GolCellState};

pub fn gol_update(
  par_commands: ParallelCommands,
  mut query: Query<(Entity, &GolCell, &GolCellState)>,
) {
  let map: HashMap<IVec2, GolCellState> = query
    .iter()
    .map(|(_entity, cell, state)| (cell.coords.clone(), state.clone()))
    .collect();

  query.par_iter_mut().for_each(|(entity, cell, state)| {
    let neighbor_coords = cell.neighbor_coordinates();
    let neighbor_states = neighbor_coords.into_iter().filter_map(|c| map.get(&c));
    let new_state = state.next_cell_state(neighbor_states);
    if let Some(update) = (state != &new_state).then_some(new_state) {
      par_commands.command_scope(|mut cmd| {
        cmd.entity(entity).insert(update);
      });
    }
  });
}
