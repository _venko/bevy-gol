use bevy::{
  ecs::system::Query,
  render::{color::Color, view::Visibility},
  sprite::Sprite,
};

use crate::GolCellState;

pub fn color_cells(mut query: Query<(&GolCellState, &mut Visibility, &mut Sprite)>) {
  query
    .par_iter_mut()
    .for_each(|(state, mut visible, mut sprite)| {
      let color = match state.0 {
        true => Some(Color::WHITE),
        false => None,
      };
      match color {
        Some(c) => {
          sprite.color = c;
          if *visible != Visibility::Inherited {
            *visible = Visibility::Inherited;
          }
        }
        None => *visible = Visibility::Hidden,
      }
    });
}
