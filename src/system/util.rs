use crate::constant::TITLE;
use bevy::{
  diagnostic::{DiagnosticsStore, FrameTimeDiagnosticsPlugin},
  ecs::{
    query::With,
    system::{Query, Res},
  },
  window::{PrimaryWindow, Window},
};

pub fn update_window_fps_display(
  diagnostics: Res<DiagnosticsStore>,
  mut window: Query<&mut Window, With<PrimaryWindow>>,
) {
  if let Some(fps) = diagnostics.get(&FrameTimeDiagnosticsPlugin::FPS) {
    if let Some(avg) = fps.average() {
      let title = format!("{TITLE}: ({avg:.1} fps)");
      window.single_mut().title = title;
    }
  }
}
