use std::ops::{Deref, DerefMut};

use bevy::{ecs::component::Component, math::IVec2};

#[rustfmt::skip]
const NEIGHBOR_COORDINATES: [IVec2; 8] = [
  IVec2::new(-1,  1), IVec2::new(0,  1), IVec2::new(1,  1),
  IVec2::new(-1,  0),                    IVec2::new(1,  0),
  IVec2::new(-1, -1), IVec2::new(0, -1), IVec2::new(1, -1),
];

#[derive(Debug, Clone, Component)]
pub struct GolCell {
  pub coords: IVec2,
}

impl Deref for GolCell {
  type Target = IVec2;

  fn deref(&self) -> &Self::Target {
    &self.coords
  }
}

impl GolCell {
  pub fn new(coords: IVec2) -> Self {
    Self { coords }
  }

  pub fn neighbor_coordinates(&self) -> impl IntoIterator<Item = IVec2> {
    NEIGHBOR_COORDINATES.map(|c| c + self.coords)
  }
}

#[derive(Debug, Copy, Clone, Default, Eq, PartialEq, Component)]
pub struct GolCellState(pub bool);

impl Deref for GolCellState {
  type Target = bool;

  fn deref(&self) -> &Self::Target {
    &self.0
  }
}

impl DerefMut for GolCellState {
  fn deref_mut(&mut self) -> &mut Self::Target {
    &mut self.0
  }
}

impl From<bool> for GolCellState {
  fn from(value: bool) -> Self {
    Self(value)
  }
}

impl GolCellState {
  pub fn next_cell_state<'a>(&self, neighbor_cells: impl Iterator<Item = &'a Self>) -> Self {
    let alive_count = neighbor_cells.filter(|c| c.0).count();
    let alive = matches!((self.0, alive_count), (true, 2 | 3) | (false, 3));
    Self(alive)
  }
}
