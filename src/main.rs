use bevy::{diagnostic::FrameTimeDiagnosticsPlugin, prelude::*, window::close_on_esc};
use rand::*;

mod component;
mod constant;
mod system;

use component::*;
use constant::*;
use system::*;

fn main() {
  App::new()
    .add_plugins(DefaultPlugins.set(WindowPlugin {
      primary_window: Some(Window {
        title: TITLE.to_string(),
        resolution: [WIN_WIDTH, WIN_HEIGHT].into(),
        ..Default::default()
      }),
      ..Default::default()
    }))
    .add_plugins(FrameTimeDiagnosticsPlugin::default())
    .add_systems(Startup, setup)
    .add_systems(Update, update_window_fps_display)
    .add_systems(Update, (gol_update, color_cells).chain())
    .add_systems(Update, close_on_esc)
    .run();
}

fn setup(mut commands: Commands) {
  commands.spawn(Camera2dBundle::default());

  let mut rng = thread_rng();
  commands
    .spawn(SpatialBundle::from_transform(Transform::from_xyz(
      -(GRID_WIDTH * SPRITE_SIZE) / 2.,
      -(GRID_HEIGHT * SPRITE_SIZE) / 2.,
      0.,
    )))
    .with_children(|builder| {
      for y in 0..GRID_HEIGHT as usize {
        for x in 0..GRID_WIDTH as usize {
          let state = GolCellState(rng.gen_bool(1. / 3.));
          builder.spawn((
            SpriteBundle {
              sprite: Sprite {
                color: Color::rgb(0., 0., 0.),
                custom_size: Some(Vec2::splat(SPRITE_SIZE)),
                ..Default::default()
              },
              transform: Transform::from_xyz(SPRITE_SIZE * x as f32, SPRITE_SIZE * y as f32, 0.),
              ..Default::default()
            },
            GolCell::new(IVec2::new(x as i32, y as i32)),
            state,
          ));
        }
      }
    });
}
