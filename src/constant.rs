pub const SPRITE_SIZE: f32 = 1.;

pub const WIN_WIDTH: f32 = 1200.;
pub const WIN_HEIGHT: f32 = 800.;

pub const GRID_WIDTH: f32 = WIN_WIDTH / SPRITE_SIZE;
pub const GRID_HEIGHT: f32 = WIN_HEIGHT / SPRITE_SIZE;

pub const TITLE: &'static str = "Game of Life";
